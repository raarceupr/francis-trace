//
// Warning this program is "hardcoded" for fields up to 2^8!
// 

//
// Usage:
// ./reed {m} {n}
//
// Example:
// ./reed 5 5


#include <stdlib.h>
#include "galois.h"
#include <math.h>

typedef unsigned int uc;
typedef unsigned int uint;
//
// Given a number count the number of ones in its 
// binary representation.
//
uc countOnes(uc n) {
	uc ctr = 0;
	while ( n ) {
		ctr = ctr + (n & 0x1);
		n = n >> 1;
	}
	return ctr;
}

//
// Given a vector of ucs and its size, 
// prints to the stdout.
//
void print_uc_vector(const uc *a, int size){
  int i;
  for (i = 0; i < size; i++) {
	printf("%u ", a[i]);
  }
  printf("\n");
}




//
// Params:
//   a: vector that contains a combination 
//   desiredResult: desired result of the xor between x1...xn
//   evensAndOdds: the array that contains a list of all 
//       numbers from [0,2^n-1] that have even, and odd number of ones.
//   b: result array
//   size: number of elements
//
void buildPreTranspose(uc *a, uc desiredResult, uc *b, uc **evensAndOdds, int size) {
	uc mask = 0x1;
	int i;
	for (i = size - 1; i >=0 ; i--) {
		b[i] = evensAndOdds[desiredResult & mask][a[i]];
		desiredResult = desiredResult >> 1;
	}

}

//
// Builds the lookup tables from alpha to polynomial (A2P), and
// polynomial to alpha (P2A), for a field 2^w.
// 
// For example: 
// A2P[0] = 1, means that alpha^0 = 1
// P2A[4] = 2, means that x^2 = alpha^2 (becuase the polynomial 0100 is x^2)
//
void buildTables(uc *A2P, uc *P2A, uc w) {
  int i; 

  for (i = 0; i < (1 << w) ; i++) {
  	*A2P = galois_ilog(i, w);
 	*P2A = galois_log(i, w);
  	A2P++; P2A++; 
  }	
}


// Buils the powerTables for the first powerQty odd numbers
// 
// For example, if powerqty = 2, it will buils tables for the 
// powers of 3 and 5.
void buildPowerTables(const uc *A2P, const uc *P2A, uc **PTable, 
	uc *powers, int powerQty, uc w) {
  int i, p;
  uc currPower; 
  for (p = 0; p < powerQty; p++) {
  	PTable[p] = malloc(sizeof(uc) * (1 << w));
  	currPower = powers[p];
	for (i = 0; i < (1 << w) - 1; i++) {
		// int e = ( (2 * p + 3 ) * i ) % ( (1 << w) - 1);
		int e = ( currPower * i ) % ( (1 << w) - 1);
		PTable[p][A2P[i]] = A2P[e];
		printf("%d\n",p);
	}	
  }
}


// Given an array numbers, builds its binary transpose.
// a: input array
// b: output array
// n: the input array contains w numbers in the range [0,2^n-1]
//
// The output output array will consist of n numbers in the 
// range [0,2^w]
//
// For example, if the input array contains (7, 1, 2)
// The output:
//   0 0 0  = 0
//   1 0 0  = 4
//   1 0 1  = 5
//   1 1 0  = 6 
//   x1,x2,x3,x4 = 0,4,5,6

void myTranspose(uc *a, uc *b, uc n, uc w) {
	char i = 0;
	for (i = 0; i < n ; i++) b[i] = 0;
	for (i = 0; i < w ; i++) {
		uc currA = a[i];
		
		int j;
		for (j = n-1; j >= 0; j--) {
			//printf("%d %d currA: %u\n", i, j,  currA);
		  b[j] = b[j] << 1;
		  b[j] = b[j] | (currA & 0x1);
		  currA = currA >> 1;

		}
	}
}


unsigned char *gCheckBox;
unsigned long long gCtr;





uint gpow(uint a, uint b, uint w) {
	uint res = 0x1;
	int i;
	for (i = 0; i < b; i++) res = galois_single_multiply(a,res,w); 
	return res;
}


void validatePowerTable(uc **PTable, int w, uc *powers, uc powerQty) {
	int i, p;
	uc expected;
	for (p = 0; p < powerQty; p++) {

		for (i = 0; i < (1 << w); i++ ) {
			expected = gpow(i, powers[p], w); 
			if ( PTable[p][i] != expected) { 
				printf("Validate %dth power of %d, expected %d, got %d\n", 
					powers[p],i, expected, PTable[p][i]);
				exit(1);
			}
		}
	}
}



int trace(uc *A2P, uc *P2A, unsigned int n, unsigned int w) {
	unsigned int fSize = (1 << w) - 1;
	uc total = 0;
	int i = 0;
	int origN = n;
	for (i = 0; i < w; i++ ) {
		total = total ^ A2P[n];
		n = ( 2 * n ) % fSize;
	}
	// printf("alpha^%d: %d\n",origN, total);
	return total;
}

void genTraceTable(uc *A2P, uc *P2A,unsigned int w, unsigned char *TT) {
  int i = 0;
  unsigned int fSize = (1 << w);
  for (i = 0; i < fSize; i++) {
  	TT[i] =  trace(A2P, P2A, i, w);
  }	
  TT[fSize - 1] = 0;
}




// Compute f(x) for all monomials, store to memory
// 
// M[0] = f(x)
// Compute Mx2 = f(x)*f(x)
// for i = 1 to 2^(m-1)-1:
//   for j in GF(2^mn):
//       M[i][j] = f(j) = j^(2i+1) = M[i-1]*Mx2
void computeAllMono(uc *A2P, uc m, uc n ) {
	unsigned int fSize = (1 << (m*n));
	unsigned int total = 0, max = 0;
	uc tmp = 0;
	unsigned int i = 0;
	unsigned int a, b, p, x;
	unsigned int coefs;
	unsigned int numberOfAs = 1 << (m-1);


	uc *monoSqr = malloc(fSize *sizeof(uc)); 
	uc **mono = malloc( numberOfAs * sizeof(uc*));

	// Compute f(x^2)
	printf("Computing the all squares....\n");
	for (x = 0; x < (fSize - 1); x++) monoSqr[x] = (x*x) % (fSize-1);

	// verifying
	for (x = 0; x < (fSize - 1); x++) { 
		printf("%x , %x, %x\n", x, monoSqr[x], A2P[x], A2P[monoSqr[x]] );
	}

	mono[0] = NULL;

	for (uc coef = 1; coef < numberOfAs; coef++) {
		mono[coef] = malloc(fSize *sizeof(uc));
		for (x = 0; x < (fSize - 1); x++) {
			tmp = coef == 0 ? coef : mono[coef-1][x];
			mono[coef][x] = (tmp * monoSqr[x]) % (fSize-1); 
		}
	}

	free(monoSqr);

}




void foo1(uc *A2P,uc *P2A, unsigned int m, unsigned int n, unsigned char *TT) {
  unsigned int fSize = (1 << (m*n));
  unsigned int total = 0, max = 0;
  uc tmp = 0;
  unsigned int i = 0;
  unsigned int a, b, p, x;
  unsigned int coefs;
  unsigned int numberOfAs = 1 << (m-1);
  
  printf("number of as: %d ",numberOfAs);

  printf("fsize: %d ",fSize);

  unsigned int aa;
  // a is the polynomial
  for (aa = 1 << (numberOfAs - 1); aa < 1 << numberOfAs; aa++) {
	a = aa; //rand() % (1 << numberOfAs);
	total = 1;
	for (x = 0; x < (fSize - 1); x++) {
		// printf("\tx: %d\n",x);
		tmp = 0;
		coefs = a;
		b = 0;
		while (coefs > 0) {
		  if (coefs & 1) {
		  	 p = 2 * b + 1;
		  	 tmp = tmp ^ A2P[ (x*p) % (fSize - 1)];
		  }	  

	      b++; 
	      coefs = coefs >> 1;  // printf("\tto the power of %d\n", p);
	    }
	   	tmp = P2A[tmp];
	   	// printf("Poly for aa: %x is: %x\n",a,tmp);
	   	total = total + ( TT[tmp] == 0 ? 1 : -1);

	}
	max = (abs(total) > max) ? abs(total) : max;
	// printf("for poly: %x total: %d max: %d\n", a, abs(total), max);
	
  }
  printf("max: %d\n", max);

}

uc gray_encode(uc b) {
	return b ^ (b >> 1);
}

uc posOfOne(uc n) {
	// printf("got %x\n",n);
	uc pos = 0;
	while ((n & 0x1) == 0) {
		n = n >> 1;
		pos++;
	}
	// printf("return pos %x\n",pos);
	return pos;
}


void foo_gray(uc *A2P,uc *P2A, unsigned int m, unsigned int n, unsigned char *TT) {
  unsigned int fSize = (1 << (m*n));
  unsigned int total = 0, max = 0;
  uc tmp = 0;
  unsigned int i = 0;
  unsigned int a, b, p, x;
  unsigned int coefs;
  unsigned int numberOfAs = 1 << (m-1);
  
  printf("number of as: %d ",numberOfAs);

  printf("fsize: %d ",fSize);
  uc *prevPolis = malloc(sizeof(uc) * fSize);
  for (uc i=0; i<fSize;i++) prevPolis[i] = 0;
  
  uc prev_a = 0;
  for ( uc aa = (1 << numberOfAs) - 1; aa >= 1 << (numberOfAs - 1); aa--) {
  // for ( uc aa = 1 << (numberOfAs - 1); aa <= 1 << numberOfAs - 1; aa++) {
	a = gray_encode(aa); //rand() % (1 << numberOfAs);
	// printf("The diff between this and old: %x\n", prev_a ^ a);
	uc pos = posOfOne(prev_a ^ a);
	// printf("The position: %x\n",pos); 
	pos = 2*pos + 1;

	total = 1;
	for (x = 0; x < (fSize - 1); x++) {
		// printf("\tx: %d\n",x);
		tmp = 0;
		coefs = a;
		b = 0;
		// while (coefs > 0) {
		//   if (coefs & 1) {
		//   	 p = 2 * b + 1;
		//   	 tmp = tmp ^ A2P[ (x*p) % (fSize - 1)];
		//   }	  

	 //      b++; 
	 //      coefs = coefs >> 1;  // printf("\tto the power of %d\n", p);
	 //    }
		tmp = prevPolis[x] ^ A2P[ (x*pos) % (fSize - 1)];
		prevPolis[x] = tmp;
	   	tmp = P2A[tmp];
	   	// printf("Poly for aa: %x is: %x\n",a,tmp);
	   	

	   	total = total + ( TT[tmp] == 0 ? 1 : -1);

	}
	max = (abs(total) > max) ? abs(total) : max;
	// printf("for poly: %x total: %d max: %d\n", a, abs(total), max);
	// printf("aa=%x",aa);
	prev_a = a;
	
  }
  printf("max: %d, limit %x\n", max, 1 << (numberOfAs - 1));

}

int main(int argc, char **argv)  {
  unsigned int x, w, b1;
  unsigned int m,n;
  int i,j;
  srand(time(NULL));
  gCtr = 0;

  uc *powers;
  uc numPowers;

  if (argc < 3) {
  	printf("Please provide the values for m and n\n");
  	exit(1);
  }

  // sscanf(argv[1], "%u", &x);
  m = atoi(argv[1]);
  n = atoi(argv[2]);

  w = m * n;

  if (w < 1 || w > 32) { fprintf(stderr, "Bad w\n"); exit(1); }

  // Tablas de conversion

  uc *A2P = malloc(sizeof(uc) * (1 << w));
  uc *P2A = malloc(sizeof(uc) * (1 << w));
  unsigned char *TT = malloc(sizeof(unsigned char) * (1 << w));

  buildTables(A2P,P2A, w);

  genTraceTable(A2P,P2A, w, TT);
  printf("the max size of the field is: %d\n", sizeof(uc));
  
  // computeAllMono(A2P, m, n);
  // exit(1);
  // foo1(A2P,P2A, m, n,TT);

  foo_gray(A2P,P2A, m, n,TT);

  // 173760
exit(0);
 
}

/*
for poly: 9 total: 56 max: 120
for poly: a total: 88 max: 120
for poly: b total: 24 max: 120
for poly: c total: 56 max: 120
for poly: d total: 120 max: 120
for poly: e total: 24 max: 120
for poly: f total: 88 max: 120

*/